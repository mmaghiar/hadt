#!/usr/bin/python
__author__ = 'Maghiar Mihai Adrian'

import  os, os.path, argparse, tarfile, shutil, socket, time, getpass, subprocess, logging
from me2s.packaging import dric
from Crypto.PublicKey import RSA
from Crypto import Random
import string, sys, json, socket
from string import Template
import logging

##################################################################################################

'''
    The main function is the starting point of the program
'''
def inputargs():
    parser = argparse.ArgumentParser()
    parser.add_argument("-j","--jobid", dest="jobid", action="store", type=int, help="job id")
    parser.add_argument("-c","--create", dest="create", action="store", type=str, help="Path for hadoop installation")
    parser.add_argument("-d","--destroy", dest="destroy", action="store", type=str, help="Path of hadoop installion to destroy the installed instance")
    parser.add_argument("-n","--nodes", dest="nodes", action="store", type=int, help="Number of nodes")
    parser.add_argument("-v","--verbose", dest="verbose", action="store", type=int, help="Program output")
    args = parser.parse_args()
    return args

def deployJob(args):

    jobTemplate = '''#!/usr/bin/python
#@ job_name = deploy.$(jobid).$(stepid)
#@ step_name = step.$(stepid)
#@ job_type = MPICH
#@ arguments = -j $(jobid) -c {0}
#@ excutable = deployer.py
#@ initialdir = output
#@ class = large
#@ environment = COPY_ALL
#@ node_usage = not_shared
#@ node = {1}
#@ wall_clock_limit = 00:29:59
#@ tasks_per_node = 1
#@ output = varenv.$(job_name).log
#@ error = varenv.$(job_name).log
#@ queue
    '''.format(args.create,args.nodes)

    hostListFile = open(os.path.join("deploy_hadoop.ll"), "w")
    hostListFile.write(jobTemplate)
    hostListFile.close()


def main():

    log = logging.getLogger("HaDTool_Main")
    log.info("Application start ...")
    print logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    args=inputargs()
    if len(args.create) > 0:
        if args.nodes > 0:
            print "deploy started"
            deployJob(args)
            subprocess.call(["llsubmit deploy_hadoop.ll"])

        else:
            print "no nodes number specified"
    else:
       print "type of action is required"

if __name__ == '__main__':
    main()