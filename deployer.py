#!/usr/bin/python
__author__ = 'Maghiar Mihai Adrian'

import  os, os.path, argparse, tarfile, shutil, socket, time, getpass, subprocess, logging
from me2s.packaging import dric
from Crypto.PublicKey import RSA
from Crypto import Random
import string, sys, json, socket
from string import Template
import logging

##################################################################################################

class Input(object):

    URL = "http://www.eu.apache.org/dist/hadoop/common/hadoop-2.6.0/hadoop-2.6.0.tar.gz"
    tempDir = "tmp/"
    args = None
    installPath = None
    keys_folder = os.path.join(os.path.expanduser("~"), ".ssh")
    authorization_file = os.path.join(keys_folder, "authorized_keys")
    public_file_path=os.path.join(keys_folder,"id_rsa_hadoop.pub")
    private_key_file_path=os.path.join(keys_folder,"id_rsa_hadoop")

    log = logging.getLogger("Input")

    def __init__(self):
        pass

    def getArgs(self):



        parser = argparse.ArgumentParser()
        parser.add_argument("-j","--jobid", dest="jobid", action="store", type=int, help="job id")
        parser.add_argument("-c","--create", dest="create", action="store", type=str, help="Path for hadoop installation")
        parser.add_argument("-d","--destroy", dest="destroy", action="store", type=str, help="Path of hadoop installion to destroy the installed instance")
        parser.add_argument("-v","--verbose", dest="verbose", action="store", type=int, help="Program output")
        self.args = parser.parse_args()

        try:
            jobid = self.args.jobid
            print jobid
            #availableHostList = "/home/loadl/execute/head.ib.priv.hpc.uvt.ro." + str(jobid) + ".0.machinefile"
            availableHostList="hosts.txt"
            #mashinesHosts = subprocess.call([cat])
            hostListFile = open(availableHostList, "r")
            hosts = hostListFile.readlines()
            hostListFile.close()
            for host in hosts:
                try:
                    addr = socket.gethostbyname(host)
                    print addr

                    #de adaugat in json file.

                except socket.error as e:
                    print e.message
                    self.log.debug("host resolving failed")



        except subprocess.CalledProcessError as e:
            print e.output
            #if e.output.startswith('error: {'):
            #    error = json.loads(e.output[7:]) # Skip "error: "
            #    print error['code']
            #    print error['message']


    def getInstallPath(self):
        return self.installPath

    def setInstallPath(self,installationPath):
        self.installPath = installationPath

    def readJsonFile(self):
        pass

class Output(object):

    log = logging.getLogger("Output")

    def __init__(self):
        pass

    def printOutput(self):
        pass

class Cluster(object):

    clusterType = None #type of cluster single, pseudo-cluster, fully distributed.
    nodesNo = None # number of nodes

    def __init__(self):
        pass

class MachineNode(object):

    log = logging.getLogger("Machine")


    hostName = None #hostname
    hostIP = None #host ip address
    hostType = None #NameNoda, DataNode, JobTracker, Secondary NameNode TaskTracker

    def __init__(self):
        pass

    def getHostName(self):
        return self.hostName

    def setHostName(self, machineHostName):
        self.hostName = machineHostName

    def getIPAddress(self):
        return self.hostIP

    def setIPAddress(self, machineHostIP):
        self.hostIP = machineHostIP

class Configuration(object):

    log = logging.getLogger("Configuration")

    def __init__(self):
        pass

    def generate_RSA(self,bits=4096):

        #This function is used to generate the authentification cerficates for nodes communication

        self.log.debug("Generating secure key ...")

        random_generator = Random.new().read(16)
        new_key = RSA.generate(bits, e=65537)
        pubkey = new_key.publickey()
        public_key = pubkey.exportKey('OpenSSH')
        private_key = new_key.exportKey()
        #public_key = public_key + " " + self.userName + "@" + self.hostName
        public_key = public_key + " " + "hadoop@cluster"
        return private_key, public_key

    def createPaths(self, folder):
        '''
            This function is used to create all the necessary paths for installation
        '''
        if not os.path.exists(folder):
            os.makedirs(folder)

    def checkOpenPort(self, port):

        '''
            This function is used to check if a port is opened
        '''

        try:
            socks = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            socks.connect(("127.0.0.1", port))
            socks.close()
            return False

        except:
            socks.close()
            return True

    def writeToFile(self, filePath, content, method="w"):
        with open(filePath, method) as fp:
            fp.write(content)
            fp.close()

    def replaceInFile(self,newString, oldString, filename):
        pass

class Deploy(object):

    log = logging.getLogger("Deploy")
    userName = getpass.getuser()
    hostName = socket.gethostname()
    tempDir = Input.tempDir

    def __init__(self):
        pass

    def clean_install(self,tmpDir):
        '''
            This function is used to clean all the junk files from installation process
        '''
        shutil.rmtree(tmpDir)
        self.log.info("Temporary folder cleaned.")

    def create(self):
        self.log.info("Hadoop deployed. Exiting program ...")


class Destroy(object):

    '''
        This class will completly destroy the deployed instance

        @:method stop - will stop all running processes
        @:method clean - will clean or remove all files that are belong to the installation folder
    '''

    log = logging.getLogger("Destroy")
    installPath = None

    def __init__(self, installLoc):
        self.installPath = installLoc
        self.log.info("Hadoop instance terminated.")

    def stop(self):
        subprocess.call([os.path.join(self.installPath, "hadoop/sbin/stop-dfs.sh")])
        subprocess.call([os.path.join(self.installPath, "hadoop/sbin/stop-yarn.sh")])

    def clean(self):

        #this method will clean extra files needed by hadoop
        if(os.path.exists(self.installPath)):
            #shutil.rmtree(self.installPath)

            '''
                This removes the authentification keys created for hadoop
            '''

            f = open(Input.authorization_file, "r")
            lines = f.readlines()
            f.close()
            #And reopen it in write mode:
            f = open(Input.authorization_file, "w")
            keyName = ("hadoop@cluster")

            for line in lines:
                if (keyName in line):
                    pass
                else:
                    f.write(line)

            f.close()

            sshconf = os.path.join(Input.keys_folder, "config")

            if(os.path.isfile(sshconf) == True):
                os.rename(sshconf + "_bak", sshconf)

            os.remove(sshconf)
            os.remove(Input.public_file_path)
            os.remove(Input.private_key_file_path)

            self.log.info("SSH authentification files cleaned.")
        else:
            self.log.info("No hadoop instance installed")



    def terminate(self):

        #self.stop()
        self.clean()

class AppController(object):

    '''
    This will control the main functionality of the deployment program
    '''
    log = logging.getLogger("AppController")

    installPath = None
    input = None
    output = None

    def __init__(self, Input, Output):
        self.input = Input
        self.output = Output

    def run(self):

        self.input.getArgs()

        try:
            if len(self.input.args.create) > 0:
                print self.input.args.create
                self.installPath = self.input.args.create

                #<<<<<<<<<<<<<<<<<<<<<<<<<------------------------------------------------------
                #hadtool.installDir = self.input.create
                #hadtool.create(self.input.URL, self.input.temp_dir, self.input.create)

                deploy = Deploy()
                deploy.create(self.installPath)

            else:
                self.log.info("No arguments supplied. Exiting program ...")
                sys.exit()
        except:
            pass

        try:
            if len(self.input.args.destroy) > 0:

                if(self.input.args.destroy == ".." or self.input.args.destroy == "../"):
                    print "You cannot destroy your own folder."
                else:
                    self.installPath = self.input.args.destroy
                    destroy = Destroy()
                    destroy.terminate(self.installPath)
            else:

                self.log.info("No arguments supplied. Exiting program ...")

        except:
            pass

'''
    The main function is the starting point of the program
'''

def main():

    log = logging.getLogger("HaDTool_Main")
    log.info("Application start ...")
    print logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    input = Input()
    output = Output()
    hadtool = AppController(input, output)
    hadtool.run()

if __name__ == '__main__':
    main()